# Nala



### Nala 

Nala jest superowym programem do zarządzania pakietami w dystrybucjach wykorzystujących `libapt-pkg` jako manager pakietów. Jego działanie przypomina mi nieco manager Homebrew, który opisałem [tutaj]({{< relref "Homebrew.md" >}}), ale o tym później.

Nala przeznaczony jest głównie dla początkujących użytkowników apta, nie ma zaimplementowanych pewnych mechanizmów, za to posiada sporo zalet, właśnie dla mniej zaawansowanych użytkowników, jak na przykład ja. :(far fa-face-grin-squint-tears):

### Dlaczego Nala?

Otóż polecenia `apt`, `apt-get`, czy `aptitude` często podczas zarządzaniem pakietami wyświetla sporo informacji, które często nie są zbytnio dla nas zrozumiałe lub wyświetlone w sposób nie do końca przejrzysty.
Posłużę się tu przykładem upgrade pakietów przy pomocy apt-geta. Przede wszystkim przed upgradem należy odświeżyć listę pakietów (mam ustawione pobieranie z serwerów lustrzanych), co wyrzuca nam coś takiego:

![apt-get update](/img/Nala/aptget1.png)

Proces uaktualniania pakietów wygląda tak:

![apt-get upgrade](/img/Nala/aptget2.png)

Przyznacie, że nie wygląda to zachęcająco, uaktualniane pakiety są "wyplute" na konsolę i trzeba się przebijać przez ten gąszcz, żeby dowiedzieć się co i jak będzie aktualizowane.

No to teraz dla porównania Nala: 

Przede wszystkim nie ma potrzeby wykonywania dwóch poleceń. Wystarczy wpisać `sudo nala upgrade` a program wykona `update` automatycznie. 

Poza tym otrzymamy ładną tabelkę z wynikiem polecenia.
  
{{< figure align=center src="/img/Nala/nala-upgrade1.png" title="Nala - uaktualnienie rpozytoriów">}}

{{< figure align=center src="/img/Nala/nala-upgrade2.png" title="pakiety które mogą zostać zaktualizowane">}}

Ta tabelka jest dłuższa, o automatyczne usuwanie, instalowanie zależności, aktualizację, i podsumowanie, ale wszystkie informacje są wyświetlone w ten właśnie sposób.

{{< figure align=center src="/img/Nala/nala-upgrade5.png" title="podsumowanie">}}

Super - zdecydowanie lepiej się to czyta i można szybko przejrzeć co się dzieje.

### Inne przydatne funkcje

Aby wywołać listę dostępnych funkcji i przełączników wystarczy wpisać komendę `sudo nala` bez żadnych dodatkowych poleceń.

Jak już wspomniałem Nala obsługuje pobieranie równoległe pobieranie -pakiety jednocześnie pobierane są z trzech serwerów lustrzanych. Po wydaniu komendy: `sudo nala fetch` zostaną przeskanowane dostępne serwery i zostaną zaprezentowane te najszybsze dla Twojej lokalizacji.

{{< figure align=center src="/img/Nala/fetch.png" title="">}}

Szok i niedowierzanie :(far fa-face-surprise): dostajemy wynik w tabelce. Możemy wybrać wszystkie 16 serwerów, a program będzie dokonywał wyboru które trzy spośród nich wybrać do jednoczesnego pobierania.

Inną ciekawą komendą jest `nala history`, która odpowiada za pokazanie jakie programy instalowaliśmy w systemie.

{{< figure align=center src="/img/Nala/history.png" title="">}}

Teraz najlepsza część. Są dodatkowe przełączniki: `nala history undo <ID>` pozwala na cofnięcie się do poprzedniej wersji danego pakietu, na przykład gdy obecnie zainstalowana ma jakieś problemy z działaniem, oraz drugi przełącznik `nala history redo <ID>` - czyli cofnięcie poprzedniej zmiany.

Kiedy potrzebujemy wyczyścić historię dla jakiegoś pakietu, służy do tego `nala history clear <ID>` dla pojedynczego, lub `--all`dla wszystkich zainstalowanych pakietów.

### Wady

Nala jak wszystko w życiu ma swoje wady. Podstawową wadą jest ograniczona ilość komend w porównaniu do innych frontendów. Nie znajdziemy tu `full-upgrade` z apta, czy `safe-upgrade` do bezpiecznego instalowania pakietów z aptitude. Niemniej jednak do codziennego użytku przez nas, niekoniecznie zaawansowanych użytkowników jest to idealne rozwiązanie, aby nie zniechęcić się do konsoli.

###### Post Scriptum

Jeśli uważasz, ze moja praca ma sens. możesz postawić mi kawę dzięki Ko-fi

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/N4N4DGJUL)


{{< script >}}
<script async src="https://comments.app/js/widget.js?3" data-comments-app-website="AVVAbk_v" data-limit="5" data-page-id="https://blogowo.codeberg.page" data-color="CA9C0E" data-dislikes="1" data-colorful="1"></script>
{{< /script >}}
