# Ripgrep




### Ripgrep

Jest to narzędzie wiersza poleceń do wyszukiwania wzorców regularnych w plikach. Może również rekurencyjnie przeszukiwać zadany katalog w poszukiwaniu wzorca.

Tu przykładowe zapytanie o "regrep" w katalogu man ![regrep](/img/ripgrep/grep.png)

Osobiście używam go do bardzo trywialnych celów - czyli wyszukiwania potrzebnych mi kawałków kodu, które gdzieś przewalają się po różnych plikach :smail:

Oczywiście narzędzie te zawiera mnóstwo opcji i przełączników, które pozwalają nam uściślić i rozbudować zapytania, a których nie jestem w stanie zapamiętać . Po lekcje odsyłam do [bloga Andrew Galanta](https://blog.burntsushi.net/ripgrep/), oraz na oficjalne [repo na github](https://github.com/BurntSushi/ripgrep) Znajdziecie tam też info o repozytoriach i sposobach instalacji do wszystkich możliwych systemów, od FreeBsd, przez Windowsa, po MacOS.

Teraz szybka instalacja w moich ulubionych systemach.

* MacOS

Dla browarników:

`$ brew install ripgrep`
	
Dla portowców:

    $ sudo port install ripgrep

*  [Sparky Linux 7](https://sparkylinux.org/) 
	
`$sudo nala install ripgrep`

lub

`$ sudo apt-get install ripgrep`
	
P.S. lubiącym widzić co się dzieje w czasie instalacji i początkującym uzytkownikom, zdecydowanie polecam zainstalowanie  [Nala](https://gitlab.com/volian/nala). To taki frontend dla libapt-pkg, jak apt-get, czy aptitude, tylko ładniejszy. Wpis o Nala ukaże się wkrótce

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/N4N4DGJUL)


{{< script >}}
<script async src="https://comments.app/js/widget.js?3" data-comments-app-website="AVVAbk_v" data-limit="5" data-page-id="https://blogowo.codeberg.page" data-color="CA9C0E" data-dislikes="1" data-colorful="1"></script>
{{< /script >}}
