# Zaloguj się kluczem Yubico

## Yubi zamiast haseł
O potrzebie wykorzystywania kluczy u2f jako dodatkowego uwierzytelnienia, chyba nie muszę nikogo przekonywać, prawda? Jeśli nie jesteś przekonany, albo nie bardzo się orientujesz, co i jak, obejrzyj [ten filmik](https://youtu.be/Zr0PffkN09w) od Niebezpiecznika. Dowiesz się po co to, dlaczego i jak?

Siedzę sobie któregoś dnia i patrzę na ten nieszczęsny kluczyk Yubico i zastanawiam się coby tu jeszcze z nim zrobić, bo wszystko co się dało już zabezpieczyłem i zasadniczo klucz marnuje roboczogodziny. A gdyby tak użyć go jako zamiennik hasła do sudo? Szybkie wyszukiwanie i okazuje się, że wcale nie jest to jakieś skomplikowane.

Instalacja potrzebnych bibliotek (Mam Debiana i na jego przykładzie będę pokazywał co i jak. W Twoim systemie może to działać trochę inaczej) jest banalna w Debianie.

Na początek sprawdzenie czy klucz jest widoczny w systemie (najpierw go włóż do USB )
`lsusb | grep U2F`     

Konsola powinna wyrzucić coś w tym stylu: 
`Bus 002 Device 022: ID 1088:0768 Yubico.com Yubikey Touch U2F Security Key`

Doinstalowałem bibliotekę obsługującą klucze `libpam-u2f`, oraz narzędzie `pamu2fcfg`
`sudo nala install libpam-u2f libpam-u2f`
(o programie Nala pisałem [wcześniej]({{< relref "Nala.md" >}})). Jeśli w twoim systemie nie ma tych pakietów, to sprawdź na stronie developer programm [Yubico](https://developers.yubico.com), jak je zainstalować, albo zajrzyj [tutaj](https://github.com/Yubico/pam-u2f).

W Debianie to jedno polecenie, ale w innych systemach może być trudniej, więc jeśli to ogarnąłeś, to była najtrudniejsza część całej zabawy.

Teraz należy utworzyć katalog na klucz rejestracyjny: 
`mkdir -p ~/.config/Yubico`,
oraz zarejestrować klucz poleceniem:
`pamu2fcfg >> ~/.config/Yubico/u2f_keys`

Po zarejestrowaniu trzeba powiedzieć systemowi kiedy klucz u2f ma zastępować hasło, a kiedy ma być dodatkowym uwierzytelnieniem.
Służą do tego dwa parametry:
`auth sufficient pam_u2f.so` zastępuje hasło, a `auth required pam_u2f.so` jest dodatkowym uwierzytelnieniem.
W katalogu /etc/ znajduje się plik *pam.conf* w którym można umieścić jeden z tych parametrów. Uznałem, że jest to dla mnie niewygodne, bo chciałem ustawić logowanie do systemu i `sudo` obsługiwane kluczem bez hasła, ale dla `su` ustawić "second factor".

Całość sprowadza się do edytowanie odpowiedniego pliku w */etc/pam.d/*. W zależności od zainstalowanego oprogramowania, będą tam pliki odpowiadające zalogowanie do różnych usług. Na pewno będzie tam plik *sudo* i *lightdm* albo inny odpowiedzialny z inny display manager i opcjonalnie *su*.
W moim przypadku *sudo* i wszystkie pliki liggtdm (miałem ich 3) otrzymały wpis `auth sufficient pam_u2f.so`, więc loguję się do systemu i wykonuję polecenia z uprawnieniami roota przy użyciu klucza, natomiast *su* zabezpieczyłem kluczem po podaniu hasła `auth required pam_u2f.so`.

Tu należy zwrócić uwagę, na pewien szczegół. Jeżeli w pliku, którym chcemy użyć klucza **zamiast** hasła znajduje się wpis `@include common-auth` nasz parametr musi znajdować się wyżej, przed nim. W przeciwnym przypadku system nadal będzie wymagał hasła mimo wpiętego klucza. 

W warunkach domowych, kiedy sam korzystasz z komputera używanie sudo z kluczem jest bardzo wygodnym rozwiązaniem. Już nie musisz wpisywać swojego super duper 20-znakowego hasła za każdym razem. Z drugiej strony jeśli klucz wyjmiesz, to znów pojawi się monit o hasło i możesz zalogować się jak zwykle z palca.

To samo, gdy twój upierdliwy brat grzebie w systemie i instaluje jakieś dziwności - obejdzie się smakiem, jeśli tylko odpowiednio skonfigurujesz pliki pam dla swojego display managera (należy zahashować `@include common-auth` i jeśli występuje w pliku `@include common-password`) i nie zostawisz klucza w kompie. Dla niego sudo zwyczajnie przestanie działać i nawet nie będzie wiedział dlaczego.

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/N4N4DGJUL)



