# Notatki w WhatsApp



## Notatki w WhatsApp
 czyli Mr Zuckenberg wie lepiej co jest Ci potrzebne.

#### Zapomnij o funkcjach, które są standardem gdzie indziej.

Chcesz sobie coś szybko zapisać? Jakiś link, notatkę czy cokolwiek, przejrzeć i być może później szernąć do kogoś? Często tak robię w Telegramie czy Signalu (taka praca, że muszę mieć kilka komunikatorów). Zapomnij o takim zapisywaniu jeśli masz tylko WhatsAppa. "Firma" wie co jest potrzebne użytkownikom, dlatego notatek brak.  W świecie komunikatorów WA jest odpowiednikiem Apple w smartfonach. Funkcje będące standardem w Androidach od lat, są wprowadzane z wielkim opóźnieniem przez Apple i robi się z tego wielkie halo. To samo mamy tutaj, chociażby klient na PC. Cała masa ficzerów dostępnych w Telegramie czy Signalu wprowadzana jest z wieeeelkim opóźnieniem i dodatkowo wszędzie się o tym pisze jak o jakiejś przełomowej funkcji. Aha i nie zapomnij że jesteś śledzony na każdym kroku, ale to tak w ramach bonusa. Między innymi dla tego bonusa WA jest najrzadziej wykorzystywanym przeze mnie komunikatorem i jest zainstalowany na osobnym urządzeniu (starym zdezelowanym smarfonie z rozbitym ekranem) bo brzydzę się tego, a jednak muszę używać.
{{< figure align=center src="/img/wa/wa9.png" title="Notatki w konkurencyjnych programach">}}

#### Notatki

No dobra, ponarzekałem, a teraz trzeba napisać coś konstruktywnego. Tworzenie notatek jest proste i banalne tylko trzeba było na to wpaść I najprawdopodobniej ktoś kto to odkrył przypadkowo. Co trzeba zrobić… 

- [x] Musisz utworzyć nową grupę dodać do niego do niej 1 dowolną osobę.

![](/img/wa/wa2.png)
![](/img/wa/wa3.png)

- [x] Nadajesz grupie nazwę "notatki" czy jaką sobie wybierzesz i zapisujesz. 

![](/img/wa/wa4.png)

- [x] Kiedy utworzysz już grupę wchodzisz w info grupy, usuwasz dodaną osobę z grupy I zostajesz w niej sam/a.
 dzięki temu masz grupę tylko dla siebie I możesz wysyłać do niej notatki.
 
 ![](/img/wa/wa5.png)
 
 ![](/img/wa/wa6.png)
 ![](/img/wa/wa7.png)
 
Oczywiście nie ma żadnych przeszkód, żeby w ten sposób utworzyć kilka różnych "notatkowych grup" do różnych potrzeb. 
Zawsze możesz dodać do takiej grupy inne osoby z Twoich kontaktów i dzielić się nimi ze znajomymi. Po odpowiednim skonfigurowaniu, osoby którym udostępniłeś/aś mogą na przykład nie mieć możliwości rozmawiania z Tobą ani zostawiania swoich notatek w grupie (lub mogą, jeśli im na to zezwolisz).

Moje grafiki są z desktopowego klienta WA, bo jak pisałem smartfon jest stary, zniszczony i służy tylko jako dawca numer., ale na pewno sobie poradzisz z tym również na smartfonie.

###### 

Jeśli uważasz, ze moja praca ma sens. możesz postawić mi kawę dzięki Ko-fi

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/N4N4DGJUL)


{{< script >}}
<script async src="https://comments.app/js/widget.js?3" data-comments-app-website="AVVAbk_v" data-limit="5" data-page-id="https://blogowo.codeberg.page" data-color="CA9C0E" data-dislikes="1" data-colorful="1"></script>
{{< /script >}}
